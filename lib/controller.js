'use strict';

function SlideshowCtrl($scope, $http, $timeout) {
    $http.get('/slideshow/').success(function (data) {
        $scope.slides = data.slides;
        $scope.slidearray = [];
        angular.forEach($scope.slides, function(value, key) { //Map object properties to array in order for angular filtering to work.
           $scope.slidearray.push(value);
        });

        $scope.index = 0;
        $scope.maxItems = $scope.slidearray.length;

        // Using $timeout instead of setInterval, otherwise we modify the data outside of Angular.
        $timeout($scope.updateSlideshow,0);

    });

    $scope.fadeOut = function () {
        //console.log('initiate fadeout');
        $scope.slideClassVar = "fadeSlideOut";
        $timeout($scope.updateSlideshow,1000);
    };

    $scope.updateSlideshow = function() {
        //console.log('change image');
        if ($scope.index < $scope.maxItems-1) {
            $scope.index += 1;
        }
        else {
            $scope.index = 0; //reset slideshow when reaching the last slide
        }
        $scope.query = $scope.slidearray[$scope.index].name; //Set new filter-value to update the slideshow

        $timeout($scope.fadeIn,0);
    };

    $scope.fadeIn = function() {
        //console.log('Start fade-in');
        $scope.slideClassVar = "fadeSlideIn";
        $timeout($scope.fadeOut,8000);
    }

    $scope.goToURL = function () {
        //console.log($scope.slidearray[$scope.index].url);
        window.location.href = $scope.slidearray[$scope.index].url;
    }
}

//SlideshowCtrl.$inject = ['$scope', '$http', '$timeout'];