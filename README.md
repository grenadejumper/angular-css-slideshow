#angular-css-slideshow

A simple slideshow-widget made with angular.js and css3 transitions. Full tutorial at [www.breathegeek.com](http://www.breathegeek.com/articles/how-to-make-a-slideshow-widget-using-Angular-js-and-CSS3/).

##Getting started
Make sure you have node.js (and npm) installed.

Clone this repository, install the npm dependencies and start the server.

    $ git clone https://grenadejumper@bitbucket.org/grenadejumper/angular-css-slideshow.git

    $ cd angular-css-slideshow

    $ npm install

    $ node server.js

Point your browser to 127.0.0.1:5000.
