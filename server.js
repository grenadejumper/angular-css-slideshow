/**
 * The main server-file
 */
'use strict';

var express = require('express');
var app = express();
app.engine('html', require('ejs').renderFile);

var requestHandlers = require('./requestHandlers');

app.configure(function(){
    app.set('views', __dirname + '/views'); //explicitly set view dir
    app.use("/css", express.static(__dirname + '/css')); //Serving CSS files
    app.use("/lib", express.static(__dirname + '/lib')); //Serving JavaScript files
    app.use("/img", express.static(__dirname + '/img')); //Serving image files
});

// Routes

app.get('/', function(req,res){
    requestHandlers.IndexHandler(req,res);
});

app.get('/slideshow/', function(req,res){
    requestHandlers.SlideshowHandler(req,res);
});

// Start server
var PORT = 5000;
app.listen(PORT);
console.log("Starting server on port " + PORT);
