'use strict';

var querystring = require('querystring');
var fs = require('fs');
var url = require('url');



function IndexHandler(req, res) {
    res.render('slideshow.html');   
}

function SlideshowHandler(req,res) {
    //Return JSON slideshow data
    fs.readFile(__dirname + '/slideshow/slideshow.json','utf-8',function(err,data){
        if (err) {
            throw err;
        }
        data = JSON.parse(data);
        res.writeHead(200, {'Content-Type': 'application/json'});
        res.write(JSON.stringify(data)); //Data needs to be serialized before sending
        res.end();
    });



}


exports.IndexHandler = IndexHandler;
exports.SlideshowHandler = SlideshowHandler;

